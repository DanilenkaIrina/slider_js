const root = document.getElementById('root');
const slider_prev = document.createElement('input');
const slider_next = document.createElement('input');
const slider = document.createElement('div');
const container1 = document.createElement('div');
const container2 = document.createElement('div');
const container3 = document.createElement('div');
const container4 = document.createElement('div');
const img1 = document.createElement('img');
const img2 = document.createElement('img');
const img3 = document.createElement('img');
const img4 = document.createElement('img');
const text1 = document.createElement('h2');
const text2 = document.createElement('h2');
const text3 = document.createElement('h2');
const text4 = document.createElement('h2');

slider_prev.classList.add('slider_prev');
slider_next.classList.add('slider_next');
slider.classList.add('slider');
container1.classList.add('container');
container2.classList.add('container', 'opacity');
container3.classList.add('container', 'opacity');
container4.classList.add('container', 'opacity');
img1.classList.add('slider_image');
img2.classList.add('slider_image');
img3.classList.add('slider_image');
img4.classList.add('slider_image');
text1.classList.add('slider_text');
text2.classList.add('slider_text');
text3.classList.add('slider_text');
text4.classList.add('slider_text');

slider_prev.setAttribute('type', 'image');
slider_prev.setAttribute('src', './prev.png');
slider_next.setAttribute('type', 'image');
slider_next.setAttribute('src', './next.png');
img1.setAttribute('src', './img1.jpg');
img2.setAttribute('src', './img2.jpg');
img3.setAttribute('src', './img3.jpg');
img4.setAttribute('src', './img4.jpg');

text1.innerText = 'Text #1';
text2.innerText = 'Text #2';
text3.innerText = 'Text #3';
text4.innerText = 'Text #4';

container1.append(img1, text1);
container2.append(img2, text2);
container3.append(img3, text3);
container4.append(img4, text4);

slider.append(container1, container2, container3, container4);
root.append(slider_prev, slider, slider_next);

const image = document.querySelectorAll('.container');
let currentSlider = 0;

const changeSlider = () => {
    for(let i = 0; i < image.length; i++) {
        image[i].classList.add('opacity');
    }
    image[currentSlider].classList.toggle('opacity');
}

const clickSliderNext = () => {
    if(currentSlider + 1 == image.length) {
        currentSlider = 0;
    } else {
        currentSlider++;
    }
    changeSlider();
}

const clickSliderPrev = () => {
    if(currentSlider - 1 == -1) {
        currentSlider = image.length-1;
    } else {
        currentSlider--;
    }
    changeSlider();
}

slider_prev.addEventListener('click', clickSliderPrev);
slider_next.addEventListener('click', clickSliderNext);